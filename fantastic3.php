<?php

function fantastic3($nth){
    
    if ($nth == 1){
        return 0;
    }
    else if ($nth <= 4 ){
        return 1;
    }
    else{
        return (fantastic3($nth - 1) + fantastic3($nth - 2) + fantastic3($nth - 3)) - 1;
    }

}

function countDays($dateInString)
{
    //Date format = MM.DD.YYY
    //print number of days from the beginning of the year specified in dateInString until the date represented in dateInString
    
    
    //Separate the string
    $fullDate = explode('.', $dateInString);
    
    //Check that it is a real date
    if (count($fullDate) == 3 && checkdate($fullDate[0], $fullDate[1], $fullDate[2]))
    {
        //The date checks out
        //Find out the year and get a date for January 1st
        $year = $fullDate[2];
        $startingDate = strtotime("{$year}-01-01");
        $endingDate = strtotime("{$year}/{$fullDate[0]}/{$fullDate[1]}");
        $datediff = $endingDate - $startingDate;
        echo floor($datediff/(60*60*24));
    }
    else
    {
        echo "Bad format";
    }

}

//Shipping

//Local shipping and int shipping
// Local shipping: number of items * distance * .8
// number of  items * ( local distance * .8 + international distance * 1.2)

?>